document.body.onselectstart = () => false;
const mainConteiner = document.getElementById('mainConteiner');
const mineField = document.getElementById('mineField');
const conteinerChild = mainConteiner.childNodes;
const mineFieldChild = mineField.childNodes;
let doOnce = 0;
let innerRestart = 0;
let flagCount = 0;
let randomArr = [];
const restart = document.createElement('button');
const fieldsMines = document.getElementById('fieldsMines');

//random function
const getRandomArbitrary = (min, max) => {
    let randomNum = parseInt(Math.random() * (max - min) + min);
    if (!randomArr.includes(randomNum)){
        randomArr.push(randomNum);
        return randomNum;
    } else {return getRandomArbitrary(min, max)};
}
//add restart button when your start game
const addRestartButton = () => {
        restart.id = 'restart';
        restart.style.cssText = `background-color:green;
        font-size:24px;`;
        restart.textContent = "RESTART!";
        document.querySelector('body').appendChild(restart);
    return innerRestart++
};
//when you want new game 
restart.onclick = () => {
    flagCount = Math.ceil(mineFieldChild.length / 6);
    randomArr = [];
    document.getElementById("flagtcounter").textContent = "";
    innerRestart = 0;
    doOnce = 0;
    mineField.innerHTML = "";
    startgame.onclick();
}

startgame.onclick = () => {
    if(fieldsMines.value > 20 || fieldsMines.value <= 1 || !Number(fieldsMines.value) ) {
        fieldsMines.placeholder='Please, choice 2-20';
        fieldsMines.value = "";
        return
    }
    for(let i = 0; i < 5; i++) {
        conteinerChild[i].hidden = true;
    }
   return drawBox(fieldsMines.value);
};

const drawBox = num =>{
    mineField.style.cssText = `grid-template-columns: repeat(${num}, 1fr);`;
    for(let i = 0; i < `${num * num}`; i++ ) {
        const cell = document.createElement('div');
        cell.classList.add('fas');
        mineField.appendChild(cell);
    };
    return randomMine(mineFieldChild.length);
}

const randomMine = allCell => {
    for(let i = 0; i < `${allCell / 6}`; i++) {
        let bufferColection = mineFieldChild[getRandomArbitrary(0,allCell)];
        bufferColection.classList.add('fa-bomb');
    }
    flagCount =  Math.ceil(mineFieldChild.length / 6);
    randomArr.sort((a, b) => a - b);  // ascending sort
    randomArr.forEach((item)=>{  //here we run check and inner number
        findTop(item);
        findDown(item);
        findLeft(item);
        findRight(item);
    });
    document.getElementById("flagtcounter").textContent = `Flag: ${flagCount} / ${Math.ceil(mineFieldChild.length / 6)}`;
};
//function detected mine on click
mineField.addEventListener('click', (event) => {
    const indexOfClickEl = [...mineFieldChild].indexOf(event.target); 
    if(event.target.classList.contains('fa-bomb') && doOnce === 0 && !event.target.classList.contains('flag')) {
        runBoom();
    } else if(!event.target.classList.contains('flag') && doOnce === 0 && event.target.id != "mineField") {
        cellOpening();
        if(mineFieldChild[indexOfClickEl].innerText === ""){  //only if cell no number
            checkAround(indexOfClickEl); 
        }
    };
    if(event.target.classList.contains('fas') && innerRestart === 0){addRestartButton()}; // inner button "restart"
});
//if have mine in cell
const runBoom = () =>{
    for(let i = 0; i < mineFieldChild.length; i++) {
        mineFieldChild[i].style.cssText = `background-color: #fff;`;
        event.target.style.cssText = `color:red;
        background-color: #fff;`;
    }
    return doOnce = 1;
};
//if don't have mine in cell
const cellOpening = () => {  //need recursion here
    event.target.style.cssText = `background-color: #fff;`;
    event.target.classList.add('cellCheked');
};
//flag function
mineField.addEventListener('contextmenu', (event) => {
    if(event.target.classList.contains('flag') && flagCount >= 0 && doOnce === 0){
        event.target.classList.remove('flag');
        ++flagCount;
    } else if(flagCount > 0 && doOnce === 0 && !event.target.classList.contains('cellCheked')) {
        event.target.classList.add('flag')
        flagCount--;
    };
    document.getElementById("flagtcounter").textContent = `Flag: ${flagCount} / ${Math.ceil(mineFieldChild.length / 6)}`;
    event.preventDefault();
});
//inner number radius around mine
const findTop = i => {
    if(i - fieldsMines.value >= 0 && !mineFieldChild[i - fieldsMines.value].classList.contains('fa-bomb')){
        mineFieldChild[i - fieldsMines.value].innerText = innerTextInCell(mineFieldChild[i - fieldsMines.value].innerText);
    }
        if(i - fieldsMines.value >= 0){
            findLeft(i - fieldsMines.value);
            findRight(i - fieldsMines.value);
        }
}

const findDown = i => {
    if(i + Number(fieldsMines.value) <= (mineFieldChild.length-1) && !mineFieldChild[i + Number(fieldsMines.value)].classList.contains('fa-bomb')){
        mineFieldChild[i + Number(fieldsMines.value)].innerText = innerTextInCell(mineFieldChild[i + Number(fieldsMines.value)].innerText);
    }
        if(i + Number(fieldsMines.value) <= (mineFieldChild.length-1)){
            findLeft(i + Number(fieldsMines.value));
            findRight(i + Number(fieldsMines.value));
        }
}

const findLeft = i => {
    if(i-1 >= 0 && !mineFieldChild[i-1].classList.contains('fa-bomb') && mineFieldChild[i].offsetTop === mineFieldChild[i-1].offsetTop){
        mineFieldChild[i-1].innerText = innerTextInCell(mineFieldChild[i-1].innerText);
    }
}

const findRight = i => {
    if(i+1 <= (mineFieldChild.length-1) && !mineFieldChild[i+1].classList.contains('fa-bomb') && mineFieldChild[i].offsetTop === mineFieldChild[i+1].offsetTop){
        mineFieldChild[i+1].innerText = innerTextInCell(mineFieldChild[i+1].innerText);
    }
}
//function check cell and +1 do if cell have value
const innerTextInCell = valueOfCell => {
    switch(valueOfCell) {
        case '1':return "2";
        case '2':return "3";
        case '3':return "4";
        case '4':return "5";
        case '5':return "6";
        case '6':return "7";
        default:return "1";}};

const checkAround = i => {   //need recursion for around cell
    const fVal = Number(fieldsMines.value)
    if(i-fVal >= 0  && !mineFieldChild[i-fVal].classList.contains('cellCheked')) checkAroundTop(i-fVal);
    if(i+fVal <= (mineFieldChild.length-1) && !mineFieldChild[i+fVal].classList.contains('cellCheked')) checkAroundBottom(i+fVal);
    if(i-1 >= 0 && mineFieldChild[i].offsetTop === mineFieldChild[i-1].offsetTop && !mineFieldChild[i-1].classList.contains('cellCheked')) checkAroundLeft(i-1);
    if(i+1<= (mineFieldChild.length-1) && mineFieldChild[i].offsetTop === mineFieldChild[i+1].offsetTop && !mineFieldChild[i+1].classList.contains('cellCheked')) checkAroundRight(i+1);
};

const checkAroundTop = iTop => {
    if(!mineFieldChild[iTop].classList.contains('fa-bomb') && mineFieldChild[iTop].innerText === ""){
        mineFieldChild[iTop].style.cssText = `background-color: #fff;`;
        mineFieldChild[iTop].classList.add('cellCheked');
        checkAround(iTop);
    } else if(!mineFieldChild[iTop].classList.contains('fa-bomb')){
        mineFieldChild[iTop].style.cssText = `background-color: #fff;`;
        mineFieldChild[iTop].classList.add('cellCheked');
    }
}

const checkAroundBottom = iBottom => {
    if(!mineFieldChild[iBottom].classList.contains('fa-bomb') && mineFieldChild[iBottom].innerText === ""){
        mineFieldChild[iBottom].style.cssText = `background-color: #fff;`;
        mineFieldChild[iBottom].classList.add('cellCheked');
        checkAround(iBottom);
    }else if(!mineFieldChild[iBottom].classList.contains('fa-bomb')){
        mineFieldChild[iBottom].style.cssText = `background-color: #fff;`;
        mineFieldChild[iBottom].classList.add('cellCheked');
    }
}

const checkAroundLeft = iLeft => {
    if(!mineFieldChild[iLeft].classList.contains('fa-bomb') && mineFieldChild[iLeft].innerText === ""){
        mineFieldChild[iLeft].style.cssText = `background-color: #fff;`;
        mineFieldChild[iLeft].classList.add('cellCheked');
        checkAround(iLeft);
    }else if(!mineFieldChild[iLeft].classList.contains('fa-bomb')){
        mineFieldChild[iLeft].style.cssText = `background-color: #fff;`;
        mineFieldChild[iLeft].classList.add('cellCheked');
    }
}

const checkAroundRight = iRight => {
    if(!mineFieldChild[iRight].classList.contains('fa-bomb') && mineFieldChild[iRight].innerText === ""){
        mineFieldChild[iRight].style.cssText = `background-color: #fff;`;
        mineFieldChild[iRight].classList.add('cellCheked');
        checkAround(iRight);
    }else if(!mineFieldChild[iRight].classList.contains('fa-bomb')){
        mineFieldChild[iRight].style.cssText = `background-color: #fff;`;
        mineFieldChild[iRight].classList.add('cellCheked');
    }
}
